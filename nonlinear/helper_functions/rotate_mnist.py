import matplotlib.pyplot as plt
from tensorflow.keras.datasets import mnist
import numpy as np
from scipy import ndimage, misc

def getRotatedDigits(digit=3):
    """
    Parameters
    ----------
    digit : number, [0 -- 9]
        DESCRIPTION. The default is 1.
        chooses which number of the mnist-dataset should be rotated

    Returns
    -------
    X : array of rotates images from mnist, contains only one digit (defined in input 'digit')
        DESCRIPTION.
    rotation : number between -180 and 180
        Degree of rotation of image.
    """
    
    (X, y), (_, _) = mnist.load_data()
    
    digit_inds = np.where(y==digit)[0]
    X = X[digit_inds]
    
    rotation = np.random.uniform(-180,180,len(X))
    
    X = np.array([ndimage.rotate(x, rot, reshape=False) for x, rot in zip(X, rotation)])
    
    return X, rotation

if __name__=='__main__':
    (X, y), (_, _) = mnist.load_data()
    plt.imshow(X[200,:,:], cmap = plt.get_cmap('gist_gray'))
    plt.xticks([])
    plt.yticks([])
    
    #X, rot = getRotatedDigits()
    #for i in range(20):
        #plt.imshow(X[i])
        #plt.show()
        #print(rot[i])