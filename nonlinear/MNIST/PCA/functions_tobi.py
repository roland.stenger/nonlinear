from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import matplotlib.pyplot as plt
import numpy as np

import matplotlib as mpl

def imscatter(x, y, image, ax=None, zoom=1):
    plt.style.use('default')
    mpl.rc('image', cmap='gray_r')
    if ax is None:
        ax = plt.gca()
    try:
        image = plt.imread(image)
    except TypeError:
        # Likely already an array...
        pass
    im = OffsetImage(image, zoom=zoom)
    x, y = np.atleast_1d(x, y)
    artists = []
    for x0, y0 in zip(x, y):
        ab = AnnotationBbox(im, (x0, y0), xycoords='data', frameon=False)
        artists.append(ax.add_artist(ab))
    ax.update_datalim(np.column_stack([x, y]))
    ax.autoscale()
    return artists
    
def image_plot(data, images_size, images, name_of_plot):	
    """image_plot(data, images_size, images, name_of_plot)
    data: finale data to plot (2D)
    images_size: size of the images in the final plot( start with 1)
    images: the images that belong to the data
    name_of_plot: name of the plot for saving
    """
    fig, ax = plt.subplots(figsize = (50,50))
    for i in range(0, len(images)):
        imscatter(data[i, 0], data[i, 1], images[i], zoom=images_size, ax=ax)
    #ax.set_ylabel('Feature 2', fontsize = 100.0)
    #ax.set_xlabel('Feature 1', fontsize = 100.0)

    ax.set_yticklabels([])
    ax.set_xticklabels([])

    plt.savefig(name_of_plot)
    plt.show()
