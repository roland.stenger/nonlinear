import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler
from sklearn.pipeline import Pipeline

from scipy import ndimage, misc


from helper_functions.rotate_mnist import getRotatedDigits
from helper_functions.functions_tobi import image_plot
from mpl_toolkits.mplot3d import Axes3D

#plt.style('ggplot')
#%%

def cart2pol(X):
    rho = np.sqrt(X[:,0]**2 + X[:,1]**2)
    phi = np.arctan2(X[:,1], X[:,0])*180/np.pi
    phi += 360
    phi %= 360
    phi -= 180
    return rho, phi

def trainMnistToRotationTransformer(X_train):
    mnistToRotationTransformer = Pipeline(steps=[
        ('pca', PCA(n_components=2)),
        ('scaler', MinMaxScaler((-1,1)))])
    
    X_train = np.reshape(X_train, (-1,28*28))    
    mnistToRotationTransformer.fit(X_train)
    
    return mnistToRotationTransformer
    
def mnistToRotation(transformer, images):
    images = np.reshape(images, (-1, 28*28))
    images_2 = transformer.transform(images)
    rho, phi = cart2pol(images_2)
    
    return rho, phi
    
def rotater(images, rotations):
    images = np.array([ndimage.rotate(x, rot, reshape=False) for x, rot in zip(images, rotations)])
    return images

def pipeline(digit=1):
    X, rot = getRotatedDigits(digit=digit)  
    transformer = trainMnistToRotationTransformer(X)
    rho, phi = mnistToRotation(transformer, X)
    images_rotated = rotater(X, phi/2-45)
    
    return images_rotated
    
#%%
for i in [1]:#,2,3,4,5,6,7,8,9]:
    images_rotated = pipeline(digit=i)
    for j in range(32):
        plt.imshow(images_rotated[j])
        plt.show()
 
#%%

for i in [1]:
    X, rot = getRotatedDigits(digit=i)
    
    X_ = np.reshape(X, (-1,28*28))
    pca = PCA(n_components=2)
    
    pca.fit(X_)
    X_pca = pca.transform(X_)
    
    mms = MinMaxScaler((-1,1))
    X_pca = mms.fit_transform(X_pca)
    
    #image_plot(X_pca[::16], 4, X[::16], f'imgplot_{i}.png')

#%%
    
rho, phi = cart2pol(X_pca)
    
#%%

#X_pca_ = X_pca[::2]
#rot_pca_ = rot[::2]
#plt.scatter(X_pca_[:,0], X_pca_[:,1], c=rot_pca_, s=20)

#%%

#fig = plt.figure()
#ax = fig.add_subplot(111, projection='3d')
#ax.scatter(X_pca_[:,0], X_pca_[:,1], rot_pca_, c=rot_pca_)

#%%