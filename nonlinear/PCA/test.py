import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.datasets import mnist
from pca import PCATransformer
from sklearn.decomposition import PCA
(X_train, y_train), (X_test, y_test) = mnist.load_data()

X_train = np.reshape(X_train, (60000, 28*28))
X_test = np.reshape(X_test, (10000, 28*28))
X_train = X_train.astype('float32') / 255.
X_test = X_test.astype('float32') / 255.

#%%
pca = PCATransformer(n_components=2)

pca_sklearn = PCA(n_components=2)

X_pca = pca.fit_transform(X_test)
X_pca_sklearn = pca_sklearn.fit_transform(X_test)

#%%
plt.scatter(X_pca[:,0], X_pca[:,1], s=0.5, c=y_test)
plt.show()
plt.scatter(X_pca_sklearn[:,0], X_pca_sklearn[:,1], s=0.5, c=y_test)