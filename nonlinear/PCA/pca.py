import numpy as np
import matplotlib.pyplot as plt
from sklearn.base import BaseEstimator, TransformerMixin

class PCATransformer(TransformerMixin, BaseEstimator):

    def __init__(self, n_components=5):
        self.n_components = n_components

    def fit(self, X):
        self.mean_ = np.mean(X.T, axis=1)
        X_centered = X - self.mean_
        
        # calculate covariance matrix
        cov = np.cov(X_centered.T)
        
        # calculate eigenvalues, eigenvectors
        val, vec = np.linalg.eigh(cov)

        sorted_inds = np.flip(np.argsort(np.abs(val)))
        self.val_sorted = val[sorted_inds]
        self.vec_sorted = vec[:,sorted_inds]
        
        #self.vec_sorted.dot()
    
        return self

    def transform(self, X):
        return X.dot(self.vec_sorted)[:,:self.n_components]
