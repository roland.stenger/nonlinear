from os.path import dirname, join as pjoin
import scipy.io as sio

matstruct_fname = pjoin('A0001.mat')
matstruct_contents = sio.loadmat(matstruct_fname)
print(matstruct_contents)
